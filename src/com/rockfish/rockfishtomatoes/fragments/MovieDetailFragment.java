package com.rockfish.rockfishtomatoes.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;

import com.rockfish.rockfishtomatoes.ImageCache;
import com.rockfish.rockfishtomatoes.MovieDetailActivity;
import com.rockfish.rockfishtomatoes.R;
import com.rockfish.rockfishtomatoes.model.Movie;

public class MovieDetailFragment extends Fragment {
	
	@InjectView(R.id.movieThumb) ImageView thumb;
	@InjectView(R.id.movieTitle) TextView view;
	@InjectView(R.id.movieRating) RatingBar movieRating;
	@InjectView(R.id.mpaaRating) TextView mpaaRating;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_movie_detail, container, false);
        
        ButterKnife.inject(this, view);
        
        return view;
    }
    
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
    	super.onActivityCreated(savedInstanceState);

		Bundle args = getArguments();
		if (args != null) {
			Movie movie = (Movie) args.getParcelable(MovieDetailActivity.EXTRA_URL);
			showMovieDetail(movie);
		}
    }

    public void showMovieDetail(Movie movie) {
        thumb.setImageDrawable(ImageCache.getInstance().getImageFromCache(movie.getPosters().original));
        view.setText(movie.getMovieTitle());
        movieRating.setRating((float) movie.getRatings().critics_score / 20);
        mpaaRating.setText(movie.getMpaaRating());
        if (movie.getMpaaRating().equalsIgnoreCase("G")) {
            mpaaRating.setTextColor(getActivity().getResources().getColor(R.color.green));
        } else if (movie.getMpaaRating().equalsIgnoreCase("PG")) {
            mpaaRating.setTextColor(getActivity().getResources().getColor(R.color.yellow));
        } else if (movie.getMpaaRating().equalsIgnoreCase("PG-13")) {
            mpaaRating.setTextColor(getActivity().getResources().getColor(R.color.orange));
        } else if (movie.getMpaaRating().equalsIgnoreCase("R")) {
            mpaaRating.setTextColor(getActivity().getResources().getColor(R.color.red));
        } else {
            mpaaRating.setTextColor(getActivity().getResources().getColor(android.R.color.black));
        }
    }

}
