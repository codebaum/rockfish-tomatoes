package com.rockfish.rockfishtomatoes;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.rockfish.rockfishtomatoes.fragments.MovieDetailFragment;
import com.rockfish.rockfishtomatoes.fragments.MovieListFragment;
import com.rockfish.rockfishtomatoes.model.Movie;

public class MovieListActivity extends FragmentActivity implements MovieListFragment.OnItemSelectedListener {

    private static final String TAG = MovieListActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.v(TAG, "onCreate()");
        setContentView(R.layout.activity_movie_list);
    }

    public void onMovieItemSelected(Movie movie) {
        Log.v(TAG, "onMovieItemSelected()");
        MovieDetailFragment fragment = (MovieDetailFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_movie_detail);
        if (fragment != null && fragment.isInLayout()) {
            fragment.showMovieDetail(movie);
        } else {
            Intent intent = new Intent(getApplicationContext(), MovieDetailActivity.class);
            intent.putExtra(MovieDetailActivity.EXTRA_URL, movie);
            startActivity(intent);

        }
    }
}
