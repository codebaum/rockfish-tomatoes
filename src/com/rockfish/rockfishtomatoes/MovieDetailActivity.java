package com.rockfish.rockfishtomatoes;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import butterknife.ButterKnife;

import com.rockfish.rockfishtomatoes.fragments.MovieDetailFragment;
import com.rockfish.rockfishtomatoes.model.Movie;

public class MovieDetailActivity extends FragmentActivity {

    public static final String EXTRA_URL = "url";
    private static final String TAG = MovieDetailActivity.class.getSimpleName();
	
//	@InjectView(R.id.movieThumb) ImageView thumb;
//	@InjectView(R.id.movieTitle) TextView view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.v(TAG, "onCreate()");
        
        ButterKnife.inject(this);

        // Need to check if Activity has been switched to landscape mode
        // If yes, finished and go back to the start Activity
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            finish();
            return;
        }
        
        setContentView(R.layout.activity_movie_detail);
        
      Bundle extras = getIntent().getExtras();
      if (extras != null) {
          Fragment newFragment = new MovieDetailFragment();
          newFragment.setArguments(extras);
          FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
          ft.replace(R.id.fragment_movie_detail, newFragment).commit();
      }
        
//        Bundle extras = getIntent().getExtras();
//        if (extras != null) {
//            Movie movie = (Movie) extras.getParcelable(EXTRA_URL);
//            view.setText(movie.getMovieTitle());
//            thumb.setImageDrawable(ImageCache.getInstance().getImageFromCache(movie.getPosters().thumbnail));
//        }
    }

}
