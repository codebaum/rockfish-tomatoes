package com.rockfish.rockfishtomatoes.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Posters implements Parcelable {

    @SerializedName("thumbnail")
    public String thumbnail;
    
    @SerializedName("detailed")
    public String original;

    public Posters(Parcel in) {
        readFromParcel(in);
    }

    public static final Parcelable.Creator<Posters> CREATOR = new Parcelable.Creator<Posters>() {

        @Override
        public Posters createFromParcel(Parcel source) {
            return new Posters(source);
        }

        @Override
        public Posters[] newArray(int size) {
            return new Posters[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.thumbnail);
        dest.writeString(this.original);
    }

    private void readFromParcel(Parcel in) {
        this.thumbnail = in.readString();
        this.original = in.readString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((thumbnail == null) ? 0 : thumbnail.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof Posters))
            return false;
        Posters other = (Posters) obj;
        if (thumbnail == null) {
            if (other.thumbnail != null)
                return false;
        } else if (!thumbnail.equals(other.thumbnail))
            return false;
        return true;
    }

}
